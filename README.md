# Fortran Project no. 3

Project no. 3 at *Programming in Fortran Language* Course. 
Detail information about the task can be found [here](http://home.agh.edu.pl/~macwozni/fort/projekt3.pdf).

## Compilator

Project was compiled and tested with:

* **gfortran v. 5.5.0**
* **OpenCoarrays v. 2.0.0**
* **Python v. 3.6.5**
* **numpy v. 1.13.3** (installed with pip)
* **f2py v. 2**
* **doxygen v.1.8.13**

## Task 1

### Python

To compile the code, type:
```
make task1
```
It will produce `./task1/slmod.so`

To run python program, type:
```
make test_python
```

### Doxygen

To create documentation, type:
```
make docs
```
It will produce files in `./docs/`

## Task 2 and 3

To compile the code, type:
```
make task3
```

---

To run performance tests for Gaussian Elimination algorithm, type:
```
make test_elimination
```
Results will be in `./task3/elimination_res` 

---

To run performance tests for Matrix Multiplying algorithm, type:
```
make test_multiplying
```
Results will be in `./task3/multiplying_res` 

