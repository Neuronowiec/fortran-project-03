NCORES = 2

.PHONY : docs task1 task3

docs:
	doxygen dox_config

task1:
	f2py3 -c src/Serial_Library.F90 -m 'task1.slmod' && \
	mv task1/slmod.*.so task1/slmod.so
	
test_python:
	python3 task1/task1.py
	
task3:
	cd task3 && \
	caf ../src/Serial_Library.F90 ../src/Parallel_Library.F90 task3_multiply.F90 -o task3_multiply && \
	caf ../src/Serial_Library.F90 ../src/Parallel_Library.F90 task3_gauss.F90 -o task3_gauss
	
test_multiplying:
	cafrun -np $(NCORES) -oversubscribe task3/task3_multiply > task3/multiplying_res
	
test_elimination:
	cafrun -np $(NCORES) -oversubscribe task3/task3_gauss > task3/elimination_res