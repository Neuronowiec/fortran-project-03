import numpy
from slmod import serial_library

print("=== GAUSSIAN ELIMINATION ===")

# in Python, values of matrix is passed by rows
AA = [ 4, -2, 4, -2, 3, 1, 4, 2, 2, 4, 2, 1, 2, -2, 4, 2 ]
bb = [ 8, 7, 10, 2 ]

A = numpy.array(AA).reshape(4, 4)
b = numpy.array(bb).reshape(4)

print("\nBefore elimination:")
print("A:")
print(A)
print("b:")
print(b)

# it will work both way
#(nA, nB) = serial_library.gauss_elimination(A, b, 4)
(nA, nB) = serial_library.gauss_elimination(A, b)

print("\nAfter elimination:")
print("A:")
print(nA)
print("b:")
print(nB)

print("\n=== MATRIX MULTIPLY ===")


mm1 = [ 1, 2, 3, 4, 4, 3, 2, 1 ]
mm2 = [ 2, 11, 1, 3, 17, 2, 5, 19, 3, 7, 23, 4 ]

m1 = numpy.array(mm1).reshape(2, 4)
m2 = numpy.array(mm2).reshape(4, 3)

print("A:")
print(m1)
print("B:")
print(m2)

# it will work both way
#m3  =serial_library.matrix_multiply(m1, m2, 2, 4, 3)
m3 = serial_library.matrix_multiply(m1, m2)

print("A*B:")
print(m3)